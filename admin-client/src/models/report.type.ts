export type ReportModel = {
    labels: []; // ,
    datasets: [],
    tables: TableReportModel[]
}


export type TableReportModel = {
    year: string;
    month: string;
    date?: string;
    sum: number;
}
