export type ProductGroupModel = {
    uuid?: string; // รหัส Type
    icon: string; // logo 
    querySearch: string;
    typeName: string; // ชื่อ
    showFor: string; // แสดงใน userStandard ปกติ หรือ userRegister หรือ all
}

