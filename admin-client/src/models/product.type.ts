export type ProductModel = {
    uuid?: string;
    productId: string; // รหัสสินค้า
    productName: string; // ชื่อสินค้า
    productDesc: string; // รายละเอียดสินค้า
    productOptions: ProductOptionModel[],
    image: ProcuteImgModel[]; // รูปภาพสินค้ามีได้หลายรูป
    discount: number; // ราคาส่วนลดสินค้า คิดเป็นเปอร์เซ็นต์
    price: number; // ราคาสินค้า
    selected: boolean; // ถูกเลือกหรือไม่ถูกเลือก
    productType: string[]; // ประเภทสินค้า
    group: string[]; // หมวดหมู่ ขายดี ลดราคา ได้รับความนิยม 
    amount?: number; // จำนวนชิ้นที่เลือก
    sumPrice?: number; // ราคารวม
    postDate?: string; // วันที่ลงสินค้า
}

export type ProcuteImgModel = {
    imageId: string;
    url: string;
}

export type ProductOptionModel = {
    productName: string;
    instock: number; // จำนวนคงเหลือในสต๊อก
}
