import type { AddressModel } from "./address.type";

export type UserFormModel = {
    uuid?: string;
    userId: string;
    loginBy: string;
    fname: string;
    lname: string;
    sex: string;
    pass: string;
    birth: string;
    email: string;
    image: string;
    telNo: string;
    address: AddressModel[];
    registerDate?: number,
    status: string;
}


export function userFormRegisStoretoJson(val: any): UserFormModel {
    const jv: UserFormModel = JSON.parse(val);
    return jv;
}