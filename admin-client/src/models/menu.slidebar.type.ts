import { faUser, IconDefinition } from "@fortawesome/free-solid-svg-icons";
import { Page } from "src/page";

export type MenuSlideBarModel = {
    id: number;
    name: string;
    icon: IconDefinition;
    page: Page;
};


export function slideBarSelectedtoJson(val: any): MenuSlideBarModel {
    let jv: MenuSlideBarModel = { id: 0, name: "บันทึกประวัติ", icon: faUser, page: Page["DASHBOARD"] };
    if (val && Object.values(val).length) {
        jv = JSON.parse(val);
    }
    return jv;
}
