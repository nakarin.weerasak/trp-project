export type ProductTypeModel = {
    uuid?: string; // รหัส Type
    icon: string; // logo 
    typeName: string; // ชื่อประเภท
}

