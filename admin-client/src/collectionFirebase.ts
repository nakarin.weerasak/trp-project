export enum Collection {
    USERS = "users",
    BILLORDERS = "billorders",
    PRODUCTS = "products",
    PRODUCTGROUPS = "product_groups",
    PRODUCTTYPES = "product_types",
    NOTIFYS = "notifys",
}

