import axios from 'axios';
import { showLazyLoading } from 'src/store';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';
import { Collection } from 'src/collectionFirebase';
import type { BillOrderModel } from 'src/models/bill.order.type';

export async function updateBillOrderAPI(billOrder: BillOrderModel) {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.post(`${APIPath.firebaseUpdate}?col=${Collection.BILLORDERS}`, billOrder, axiosHeaderOptions());
        showLazyLoading.set(false);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            return respAPI?.data;
        } else {
            return respAPI?.data;
        }
    } catch (error) {
        showLazyLoading.set(false);
        return {}
    }
}
