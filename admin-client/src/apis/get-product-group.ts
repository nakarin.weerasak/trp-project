import axios from 'axios';
import { apiError, showLazyLoading, productGroupListStore } from 'src/store';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';
import { Collection } from 'src/collectionFirebase';

export async function getProductGroupAPI() {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.get(`${APIPath.firebaseGet}?col=${Collection.PRODUCTGROUPS}`, axiosHeaderOptions());
        setTimeout(() => {
            showLazyLoading.set(false);
        }, 1000);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            productGroupListStore.set(respAPI?.data?.data || {});
        } else {
            apiError.set(respAPI?.data)
            productGroupListStore.set([]);
        }
    } catch (error) {
        apiError.set(error)
        setTimeout(() => {
            showLazyLoading.set(false);
        }, 1000);
    }
}
