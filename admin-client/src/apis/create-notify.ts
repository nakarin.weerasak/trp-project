import axios from 'axios';
import { showLazyLoading, userFormRegisStore } from 'src/store';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';

export async function createNotifyAPI(userId: string, message: string) {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.post(`${APIPath.createNotify}`, {
            message: message,
            userId: userId
        }, axiosHeaderOptions());
        showLazyLoading.set(false);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            userFormRegisStore.set(respAPI?.data || {});
            return respAPI?.data;
        } else {
            return respAPI?.data;
        }
    } catch (error) {
        showLazyLoading.set(false);
        return {}
    }
}
