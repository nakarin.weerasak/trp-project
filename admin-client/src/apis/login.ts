import axios from 'axios';
import { APIPath } from 'src/api-path';
import { apiError, isAuthSuccess, showLazyLoading, userProfileStore } from 'src/store';

export async function loginAPI(_username: String, _password: String) {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.post(APIPath.login, { username: _username, password: _password });
        setTimeout(() => {
            showLazyLoading.set(false);
        }, 1000);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            userProfileStore.set(respAPI?.data?.data || {});
            if (respAPI?.data?.data?.username) {
                isAuthSuccess.set(true);
            }
            return (respAPI?.data || {})['data'];
        } else {
            apiError.set(respAPI?.data)
            userProfileStore.set({ username: "", status: "" });
            isAuthSuccess.set(false);
            return (respAPI?.data || {});
        }
    } catch (error) {
        apiError.set(error)
        setTimeout(() => {
            showLazyLoading.set(false);
        }, 1000);
        return { "message": "* Incorrect username or password" };
    }
}
