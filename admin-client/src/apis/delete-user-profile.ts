import axios from 'axios';
import { apiError, showLazyLoading, userDataListStore } from 'src/store';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';
import type { UserFormModel } from 'src/models/user.form.type';

export async function deleteUserProfileAPI(uuid: string) {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.delete(`${APIPath.userProfile}?uuid=${uuid}`, axiosHeaderOptions());
        setTimeout(() => {
            showLazyLoading.set(false);
        }, 1000);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            userDataListStore.set(respAPI?.data?.data || {});
        } else {
            apiError.set(respAPI?.data)
            userDataListStore.set([]);
        }
    } catch (error) {
        apiError.set(error)
        setTimeout(() => {
            showLazyLoading.set(false);
        }, 1000);
    }
}
