import axios from 'axios';
import { apiError, showLazyLoading, productListStore } from 'src/store';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';
import { Collection } from 'src/collectionFirebase';

export async function getCountAPI(collectionName: string) {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.get(`${APIPath.firebaseGetCount}?col=${collectionName}`, axiosHeaderOptions());
        return respAPI?.data?.data || 0;
    } catch (error) {
        apiError.set(error)
        return 0;
    }
}
