import axios from 'axios';
import { apiError, isAuthSuccess, showLazyLoading, userProfileStore } from 'src/store';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';

export async function verifyAuthAPI() {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.get(APIPath.verifyAuth, axiosHeaderOptions());
        console.log(respAPI);
        setTimeout(() => {
            showLazyLoading.set(false);
        }, 1000);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            userProfileStore.set(respAPI?.data?.data || {});
            isAuthSuccess.set(true);
        } else {
            apiError.set(respAPI?.data)
            userProfileStore.set({ username: "", status: "" });
            setTimeout(() => {
                showLazyLoading.set(false);
            }, 1000);
        }
    } catch (error) {
        apiError.set(error)
        setTimeout(() => {
            showLazyLoading.set(false);
        }, 1000);
    }
}
