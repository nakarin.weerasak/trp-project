import axios from 'axios';
import { apiError, showLazyLoading, billOrderListStore } from 'src/store';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';
import { Collection } from 'src/collectionFirebase';

export async function getBillOrderAPI() {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.get(`${APIPath.firebaseGet}?col=${Collection.BILLORDERS}`, axiosHeaderOptions());
        setTimeout(() => {
            showLazyLoading.set(false);
        }, 1000);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            billOrderListStore.set(respAPI?.data?.data || {});
        } else {
            apiError.set(respAPI?.data)
            billOrderListStore.set([]);
        }
    } catch (error) {
        apiError.set(error)
        setTimeout(() => {
            showLazyLoading.set(false);
        }, 1000);
    }
}
