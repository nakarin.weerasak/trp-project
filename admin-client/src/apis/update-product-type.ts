import axios from 'axios';
import { showLazyLoading } from 'src/store';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';
import { Collection } from 'src/collectionFirebase';
import type { ProductTypeModel } from 'src/models/product.type.type';

export async function updateProductTypeAPI(productType: ProductTypeModel) {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.post(`${APIPath.firebaseUpdate}?col=${Collection.PRODUCTTYPES}`, productType, axiosHeaderOptions());
        showLazyLoading.set(false);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            return respAPI?.data;
        } else {
            return respAPI?.data;
        }
    } catch (error) {
        showLazyLoading.set(false);
        return {}
    }
}
