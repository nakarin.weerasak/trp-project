import axios from 'axios';
import { apiError, showLazyLoading } from 'src/store';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';
export async function getUsereReportDateAPI(date: string) {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.get(`${APIPath.getUsersReportYear}?date=${date}`, axiosHeaderOptions());
        setTimeout(() => {
            showLazyLoading.set(false);
        }, 1000);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            return respAPI?.data?.data
        } else {
            apiError.set(respAPI?.data)
            return {}
        }
    } catch (error) {
        apiError.set(error)
        setTimeout(() => {
            showLazyLoading.set(false);
        }, 1000);
    }
}
