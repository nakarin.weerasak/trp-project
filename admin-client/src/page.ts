export enum Page {
    LOGIN,
    DASHBOARD,
    PRODUCT_LIST,
    PRODUCT_MANAGE,
    USERLIST,
    SALES_REPORT,
    USERS_REPORT,
    PRODUCT_GROUP_TYPE,
    BILL_ORDER
}

