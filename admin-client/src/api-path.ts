const prex = (import.meta as any).env.SNOWPACK_PUBLIC_API_PREFIX;

export const APIPath = {
    login: `${prex}/login`,
    verifyAuth: `${prex}/verify`,
    userProfile: `${prex}/users-profile`,
    firebasePost: `${prex}/firebase-post`,
    firebaseUpdate: `${prex}/firebase-update`,
    firebaseDel: `${prex}/firebase-del`,
    firebaseGet: `${prex}/firebase-get`,
    firebaseGetCount: `${prex}/firebase-get-count`,
    createNotify: `${prex}/create-notify`,
    getSalesReportYear: `${prex}/get-sales-report`,
    getUsersReportYear: `${prex}/get-users-report`
}
