import { writable } from "svelte/store";
import type { BillOrderModel } from "./models/bill.order.type";
import { MenuSlideBarModel, slideBarSelectedtoJson } from "./models/menu.slidebar.type";
import type { ProductGroupModel } from "./models/product.group.type";
import type { ProductModel } from "./models/product.type";
import type { ProductTypeModel } from "./models/product.type.type";
import type { UserDataModel } from "./models/user.auth.type";
import { UserFormModel, userFormRegisStoretoJson } from "./models/user.form.type";
import type { Page } from "./page";

export const pageSelectedStore = writable<Page>(0);
export const showLazyLoading = writable(false);
export const isAuthSuccess = writable(false);
export const formViewMode = writable(false);
export const apiError = writable({});
export const userDataListStore = writable<UserFormModel[]>([]);
export const userFormRegisStore = writable(<UserFormModel>{});
export const userProfileStore = writable<UserDataModel>(<UserDataModel>{});
export const productListStore = writable<ProductModel[]>([]);
export const billOrderListStore = writable<BillOrderModel[]>([]);
export const productSelectedStore = writable(<ProductModel>{});
export const billOrderSelectedStore = writable(<BillOrderModel>{});
export const productGroupListStore = writable<ProductGroupModel[]>([]);
export const productTypeListStore = writable<ProductTypeModel[]>([]);

// Local Store Set

userFormRegisStore.set(userFormRegisStoretoJson(localStorage.getItem("userFormRegisStore")));
userFormRegisStore.subscribe((val: any) => {
    if (val && Object.values(val).length) {
        localStorage.setItem("userFormRegisStore", JSON.stringify(val));
    }
});
