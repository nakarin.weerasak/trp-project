const moment = (window as any).moment;
// set location momnt เป็น Thailand

export function convertFullTime(time: any) {
    return moment(time).locale('th').format('lll');
}

export function convertDateTime(time: any) {
    return moment(time).locale('th').format('ll');
}