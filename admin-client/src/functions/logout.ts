export function logoutFn() {
    var cookies = document.cookie.split(";");
    cookies.forEach(function (c) {
        document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/");
    });

    window.localStorage.clear();
    window.location.reload();
}