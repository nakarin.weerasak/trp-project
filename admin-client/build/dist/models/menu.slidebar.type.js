import {faUser} from "../../_snowpack/pkg/@fortawesome/free-solid-svg-icons.js";
import {Page} from "../page.js";
export function slideBarSelectedtoJson(val) {
  let jv = {id: 0, name: "บันทึกประวัติ", icon: faUser, page: Page["DASHBOARD"]};
  if (val && Object.values(val).length) {
    jv = JSON.parse(val);
  }
  return jv;
}
