const admin = require('firebase-admin');
const serviceAccount = require("./config.json")
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

const db = admin.firestore();

module.exports = () => {
    const FibLogin = (username, password) => {
        return new Promise(async(resolve, reject) => {
            const findByUserName = db.collection("users-admin").where("username", "==", username).get();
            const findByEmail = db.collection("users-admin").where("email", "==", username).get();
            const [userIdSnapshot, userEmailSnapshot] = await Promise.all([
                findByUserName,
                findByEmail
            ]);

            const userNameDataDocs = userIdSnapshot.docs;
            const emailDataDocs = userEmailSnapshot.docs;

            if (userNameDataDocs.length > 0) {
                for (let i = 0; i < userNameDataDocs.length; i++) {
                    const docSnapshot = userNameDataDocs[i];
                    if (docSnapshot.data()['password'] == password) {
                        resolve(docSnapshot.data())
                    }
                }
                resolve({})
            } else if (emailDataDocs.length > 0) {
                for (let i = 0; i < emailDataDocs.length; i++) {
                    const docSnapshot = emailDataDocs[i];
                    console.log(password)
                    if (docSnapshot.data()['password'] == password) {
                        resolve(docSnapshot.data())
                    }
                }
                resolve({})
            } else {
                resolve({})
            }

        });
    };

    const FibGetDataWhere = (collectionName, key, valuefind) => {
        return new Promise((resolve, reject) => {
            db.collection(collectionName).where(key, "==", valuefind).get().then(querySnapshot => {
                const dataArr = [];
                if (!querySnapshot.empty) {
                    querySnapshot.forEach(doc => {
                        dataArr.push(doc.data())
                    })
                    resolve(dataArr);
                } else {
                    resolve([])
                }
            }).catch(err => {
                reject(err);
            })
        });
    };

    const FibGetDataFind = (collectionName, key, valuefind) => {
        return new Promise((resolve, reject) => {
            db.collection(collectionName).where(key, "==", valuefind).get().then(querySnapshot => {
                if (!querySnapshot.empty) {
                    resolve(querySnapshot.docs[0].data())
                } else {
                    resolve([])
                }
            }).catch(err => {
                reject(err);
            })
        });
    };

    const FibGetDataAll = (collectionName) => {
        return new Promise((resolve, reject) => {
            db.collection(collectionName).onSnapshot(function(snapshot) {
                const data = [];
                for (let i = 0; i < snapshot.docs.length; i++) {
                    let userInfo = {};
                    userInfo = snapshot.docs[i].data();
                    userInfo = userInfo;
                    data.push(userInfo)
                }
                resolve(data);
            });
        });
    };


    const FibUpdateData = (collectionName, id, data) => {
        return new Promise((resolve, reject) => {
            db.collection(collectionName).doc(id)
                .set(data)
                .then((resp) => {
                    resolve(resp);
                })
                .catch((err) => {

                    reject(err);
                });
        });

    };

    const FibAddData = (collectionName, primaryKey, data) => {
        return new Promise((resolve, reject) => {
            db.collection(collectionName).doc(primaryKey)
                .set(Object.assign({...data, uuid: primaryKey }))
                .then((resp) => {
                    resolve(resp);
                })
                .catch((err) => {
                    console.log('error ', err);
                    reject(err);
                });
        });

    };

    const FibDelData = (collectionName, fbid) => {
        return new Promise((resolve, reject) => {
            db.collection(collectionName)
                .doc(fbid)
                .delete()
                .then((resp) => {

                    resolve(resp);
                })
                .catch((err) => {

                    reject(err);
                });
        });
    };

    return { FibLogin, FibGetDataAll, FibGetDataFind, FibGetDataWhere, FibAddData, FibDelData, FibUpdateData };
}