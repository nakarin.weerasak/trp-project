const express = require("express");
const session = require("express-session");
const cors = require("cors");
const jwt = require("express-jwt");
const jsonWebtoken = require("jsonwebtoken");
const path = require("path");
const helmet = require("helmet");
const app = express();
const { uuid } = require("uuidv4");

const JWT_SECRET = "JWT_SECRETadminP@sswordAdmin";

const router = express.Router();
// #Firebase
const {
  FibLogin,
  FibGetDataAll,
  FibAddData,
  FibDelData,
  FibGetDataWhere,
  FibGetDataFind,
  FibUpdateData,
} = require("./firebase-fn")();

// #EJS
// app.set("view engine", "ejs");

app.use(cors());
app.use(helmet({ contentSecurityPolicy: false }));
app.use(express.json({ limit: "20mb" }));
app.use(express.urlencoded({ extended: true }));
app.use(
  session({
    secret: "I_X$%SOE32%0!dP@sowo2-1d@AdminM@n@gE", // set your secret here
    saveUninitialized: false,
    resave: false,
    cookie: { maxAge: 60000 },
  })
);

function getCookie(req, name) {
  const cookie = `; ${req.headers.cookie}`;
  const parts = cookie.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(";").shift();
}

// app.use(
//   jwt({
//     secret: JWT_SECRET,
//     algorithms: ["HS256"],
//     getToken: (req) => {
//       if (
//         req.headers.authorization &&
//         req.headers.authorization.split(" ")[0] === "Bearer"
//       ) {
//         return req.headers.authorization.split(" ")[1];
//       } else if (req.query && req.query.token) {
//         return req.query.token;
//       } else if (req.headers && getCookie(req, "jwt_token")) {
//         return getCookie(req, "jwt_token");
//       }
//       return null;
//     },
//   }).unless({ path: ["/service/verify", "/service/login", "/service/auth", "/"] })
// );

app.use(express.static(path.join(__dirname, "./client")));

// prefix service
app.use("/service", router);

router.post("/login", (req, res) => {
  const body = req.body || {};
  if (body && body["username"] == "admin" && body["password"] == "P@ssw0rd") {
    const signJWT = jsonWebtoken.sign(
      {
        user: "admin",
      },
      JWT_SECRET
    );
    res.cookie("jwt_token", signJWT, {
      httpOnly: false,
    });
    res.send({
      status: "200",
      message: "success",
      data: {
        username: "admin",
      },
    });
  } else {
    res.send({
      status: "200",
      message: "success",
      data: "error",
    });
  }
});

router.get(
  "/verify",
  jwt({
    secret: JWT_SECRET,
    algorithms: ["HS256"],
    getToken: (req) => {
      if (
        req.headers.authorization &&
        req.headers.authorization.split(" ")[0] === "Bearer"
      ) {
        return req.headers.authorization.split(" ")[1];
      } else if (req.query && req.query.token) {
        return req.query.token;
      } else if (req.headers && getCookie(req, "jwt_token")) {
        return getCookie(req, "jwt_token");
      }
      return null;
    },
  }),
  async (req, res) => {
    res.send({
      status: "200",
      message: "success",
      data: req.user,
    });
  }
);

router.get("/firebase-get-count", async (req, res) => {
  try {
    if (req.query && req.query["col"]) {
      const fibData = await FibGetDataAll(req.query["col"]);
      res.send({
        status: "200",
        message: "success",
        data: fibData.length || 0,
      });
    } else {
      res.send({
        status: "200",
        message: "success",
        data: 0,
      });
    }
  } catch (err) {
    res.send({
      status: "error",
      message: "error",
      error: err,
    });
  }
});

router.get("/get-sales-report", async (req, res) => {
  try {
    if (req.query && req.query["year"]) {
      const month = [
        "มกราคม",
        "กุมภาพันธ์",
        "มีนาคม",
        "เมษายน",
        "พฤษภาคม",
        "มิถุนายน",
        "กรกฎาคม",
        "สิงหาคม",
        "กันยายน",
        "ตุลาคม",
        "พฤศจิกายน",
        "ธันวาคม",
      ];
      let reportObj = {
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,
        6: 0,
        7: 0,
        8: 0,
        9: 0,
        10: 0,
        11: 0,
        12: 0,
      };
      let dataTable = [];
      const fibData = await FibGetDataAll("billorders");
      fibData.filter((data, i) => {
        if (
          new Date(data["createTime"]).getFullYear() + 543 ==
          req.query["year"]
        ) {
          reportObj = Object.assign(reportObj, {
            [`${new Date(data["createTime"]).getMonth() + 1}`]:
              data["sumPriceBill"] +
              (reportObj[new Date(data["createTime"]).getMonth() + 1] || 0),
          });
        }
      });

      Object.keys(reportObj).filter((key) => {
        dataTable.push({
          year: req.query["year"],
          month: month[key - 1],
          sum: reportObj[key],
        });
      });

      res.send({
        status: "200",
        message: "success",
        data: {
          labels: month,
          datasets: [
            {
              label: `รายงานยอดขายปี ${req.query["year"]} คิดเป็นบาท`,
              data: Object.values(reportObj),
              backgroundColor: [
                "#E74C3C",
                "#9B59B6",
                "#2980B9",
                "#16A085",
                "#F1C40F",
                "#BDC3C7",
                "#E74C3C",
                "#9B59B6",
                "#2980B9",
                "#16A085",
                "#F1C40F",
                "#BDC3C7",
              ],
              borderWidth: 2,
              borderColor: "#F8F9F9",
            },
          ],
          tables: dataTable,
        },
      });
    } else if (req.query && req.query["date"]) {
      const fibData = await FibGetDataAll("billorders");
      let sumByDate = 0;
      fibData.filter((data, i) => {
        if (
          new Date(data["createTime"]).getFullYear() + 543 ==
            new Date(req.query["date"]).getFullYear() + 543 &&
          new Date(data["createTime"]).getMonth() ==
            new Date(req.query["date"]).getMonth() &&
          new Date(data["createTime"]).getDate() ==
            new Date(req.query["date"]).getDate()
        ) {
          sumByDate += data["sumPriceBill"];
        }
      });
      res.send({
        status: "200",
        message: "success",
        data: {
          year: new Date(req.query["date"]).getFullYear(),
          month: new Date(req.query["date"]).getMonth() + 1,
          date: new Date(req.query["date"]).getDate(),
          sum: sumByDate,
        },
      });
    } else {
      res.send({
        status: "200",
        message: "success",
        data: {
          year: new Date(req.query["date"]).getFullYear(),
          month: new Date(req.query["date"]).getMonth() + 1,
          date: new Date(req.query["date"]).getDate(),
          sum: 0,
        },
      });
    }
  } catch (err) {
    res.send({
      status: "error",
      message: "error",
      error: err,
    });
  }
});

router.get("/get-users-report", async (req, res) => {
  try {
    if (req.query && req.query["year"]) {
      const month = [
        "มกราคม",
        "กุมภาพันธ์",
        "มีนาคม",
        "เมษายน",
        "พฤษภาคม",
        "มิถุนายน",
        "กรกฎาคม",
        "สิงหาคม",
        "กันยายน",
        "ตุลาคม",
        "พฤศจิกายน",
        "ธันวาคม",
      ];
      let reportObj = {
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,
        6: 0,
        7: 0,
        8: 0,
        9: 0,
        10: 0,
        11: 0,
        12: 0,
      };
      let dataTable = [];
      const fibData = await FibGetDataAll("users");
      fibData.filter((data, i) => {
        if (
          new Date(data["registerDate"]).getFullYear() + 543 ==
          req.query["year"]
        ) {
          reportObj = Object.assign(reportObj, {
            [`${new Date(data["registerDate"]).getMonth() + 1}`]: (reportObj[new Date(data["registerDate"]).getMonth() + 1] || 0) + 1,
          });
        }
      });

      Object.keys(reportObj).filter((key) => {
        dataTable.push({
          year: req.query["year"],
          month: month[key - 1],
          sum: reportObj[key],
        });
      });

      res.send({
        status: "200",
        message: "success",
        data: {
          labels: month,
          datasets: [
            {
              label: `รายงานสมาชิกรายปี พ.ศ. ${req.query["year"]} (user)`,
              data: Object.values(reportObj),
              backgroundColor: [
                "#E74C3C",
                "#9B59B6",
                "#2980B9",
                "#16A085",
                "#F1C40F",
                "#BDC3C7",
                "#E74C3C",
                "#9B59B6",
                "#2980B9",
                "#16A085",
                "#F1C40F",
                "#BDC3C7",
              ],
              borderWidth: 2,
              borderColor: "#F8F9F9",
            },
          ],
          tables: dataTable,
        },
      });
    } else if (req.query && req.query["date"]) {
      const fibData = await FibGetDataAll("users");
      let sumByDate = 0;
      fibData.filter((data, i) => {
        if (
          new Date(data["registerDate"]).getFullYear() + 543 ==
            new Date(req.query["date"]).getFullYear() + 543 &&
          new Date(data["registerDate"]).getMonth() ==
            new Date(req.query["date"]).getMonth() &&
          new Date(data["registerDate"]).getDate() ==
            new Date(req.query["date"]).getDate()
        ) {
          sumByDate += 1;
        }
      });
      res.send({
        status: "200",
        message: "success",
        data: {
          year: new Date(req.query["date"]).getFullYear(),
          month: new Date(req.query["date"]).getMonth() + 1,
          date: new Date(req.query["date"]).getDate(),
          sum: sumByDate,
        },
      });
    } else {
      res.send({
        status: "200",
        message: "success",
        data: {
          year: new Date(req.query["date"]).getFullYear(),
          month: new Date(req.query["date"]).getMonth() + 1,
          date: new Date(req.query["date"]).getDate(),
          sum: 0,
        },
      });
    }
  } catch (err) {
    res.send({
      status: "error",
      message: "error",
      error: err,
    });
  }
});

router.get("/firebase-get", async (req, res) => {
  try {
    if (req.query && req.query["col"]) {
      const fibData = await FibGetDataAll(req.query["col"]);
      res.send({
        status: "200",
        message: "success",
        data: fibData,
      });
    } else {
      res.send({
        status: "200",
        message: "success",
        data: [],
      });
    }
  } catch (err) {
    res.send({
      status: "error",
      message: "error",
      error: err,
    });
  }
});

router.get("/get-product-group", async (req, res) => {
  try {
    if (req.query && req.query["group"].match("ลดราคา")) {
      const fibData = await FibGetDataAll("products");
      const filter = fibData.filter((product) => +product["discount"] > 0);
      res.send({
        status: "200",
        message: "success",
        data: filter,
      });
    } else if (req.query && req.query["group"]) {
      const fibData = await FibGetDataAll("products");
      const filter = fibData.filter((product) =>
        product["group"].includes(req.query["group"])
      );
      res.send({
        status: "200",
        message: "success",
        data: filter,
      });
    } else {
      res.send({
        status: "200",
        message: "success",
        data: [],
      });
    }
  } catch (err) {
    res.send({
      status: "error",
      message: "error",
      error: err,
    });
  }
});

router.get("/get-product-type", async (req, res) => {
  try {
    if (req.query && req.query["productType"]) {
      const fibData = await FibGetDataAll("products");
      const filter = fibData.filter((product) =>
        product["productType"].includes(req.query["productType"])
      );
      res.send({
        status: "200",
        message: "success",
        data: filter,
      });
    } else {
      res.send({
        status: "200",
        message: "success",
        data: [],
      });
    }
  } catch (err) {
    res.send({
      status: "error",
      message: "error",
      error: err,
    });
  }
});

router.get("/firebase-get-where", async (req, res) => {
  try {
    if (
      req.query &&
      req.query["col"] &&
      req.query["key"] &&
      req.query["valuefind"]
    ) {
      const fibData = await FibGetDataWhere(
        req.query["col"],
        req.query["key"],
        req.query["valuefind"]
      );
      res.send({
        status: "200",
        message: "success",
        data: fibData,
      });
    } else {
      res.send({
        status: "200",
        message: "success",
        data: [],
      });
    }
  } catch (err) {
    res.send({
      status: "error",
      message: "error",
      error: err,
    });
  }
});

router.get("/firebase-get-find", async (req, res) => {
  try {
    if (
      req.query &&
      req.query["col"] &&
      req.query["key"] &&
      req.query["valuefind"]
    ) {
      const fibData = await FibGetDataFind(
        req.query["col"],
        req.query["key"],
        req.query["valuefind"]
      );
      res.send({
        status: "200",
        message: "success",
        data: fibData,
      });
    } else {
      res.send({
        status: "200",
        message: "success",
        data: [],
      });
    }
  } catch (err) {
    res.send({
      status: "error",
      message: "error",
      error: err,
    });
  }
});

router.post("/create-notify", async (req, res) => {
  try {
    if (req.body && Object.values(req.body).length && req.body["message"]) {
      const fibData = await FibAddData(
        "notifys",
        `${req.query["col"]}_${uuid()}`,
        {
          userId: req.body["userId"],
          timeStamp: Date.now(),
          readed: false,
          description: req.body["message"],
        }
      );
      res.send({
        status: "200",
        message: "success",
        data: fibData,
      });
    } else {
      res.send({
        status: "200",
        message: "success",
        data: {},
      });
    }
  } catch (err) {
    res.send({
      status: "error",
      message: "error",
      error: err,
    });
  }
});

router.post("/firebase-post", async (req, res) => {
  try {
    if (req.body && Object.values(req.body).length && req.query["col"]) {
      const fibData = await FibAddData(
        req.query["col"],
        `${req.query["col"]}_${uuid()}`,
        req.body
      );
      res.send({
        status: "200",
        message: "success",
        data: fibData,
      });
    } else {
      res.send({
        status: "200",
        message: "success",
        data: {},
      });
    }
  } catch (err) {
    res.send({
      status: "error",
      message: "error",
      error: err,
    });
  }
});

router.post("/firebase-del", async (req, res) => {
  try {
    if (req.query && req.query["col"] && req.query["uuid"]) {
      const fibData = await FibDelData(req.query["col"], req.query["uuid"]);
      res.send({
        status: "200",
        message: "success",
        data: fibData,
      });
    } else {
      res.send({
        status: "200",
        message: "success",
        data: {},
      });
    }
  } catch (err) {
    res.send({
      status: "error",
      message: "error",
      error: err,
    });
  }
});

router.post("/firebase-update", async (req, res) => {
  try {
    if (
      req.body &&
      req.query &&
      req.query["col"] &&
      (req.body["uuid"] || req.query["uuid"])
    ) {
      const fibData = await FibUpdateData(
        req.query["col"],
        req.body["uuid"] || req.query["uuid"],
        req.body
      );
      res.send({
        status: "200",
        message: "success",
        data: fibData,
      });
    } else {
      res.send({
        status: "200",
        message: "success",
        data: {},
      });
    }
  } catch (err) {
    res.send({
      status: "500",
      message: "error",
      error: err,
    });
  }
});

const port = process.env.PORT || 3000;

// const httpsServer = https.createServer(credentials, app);
// httpsServer.listen(port);
app.listen(port, "0.0.0.0", () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
