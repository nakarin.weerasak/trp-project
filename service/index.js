const express = require("express");
const session = require("express-session");
const cors = require("cors");
const nodemailer = require("nodemailer");
const fs = require("fs");
const path = require("path");
const helmet = require("helmet");
const puppeteer = require("puppeteer");
const app = express();
const omise = require("omise")({
    publicKey: "pkey_test_5oc9bct7kzonpuc02uh",
    secretKey: "skey_test_5oc9bdivyn2rm5rddmo",
    omiseVersion: "2019-05-29",
});
const { uuid } = require("uuidv4");
const { dbTH } = require("./address-th");

const https = require("https");

const privateKey = fs.readFileSync("sslkey/localhost.key", "utf8");
const certificate = fs.readFileSync("sslkey/localhost.crt", "utf8");
const credentials = { key: privateKey, cert: certificate };

const router = express.Router();
// #Firebase
const {
    FibLogin,
    FibGetDataAll,
    FibAddData,
    FibDelData,
    FibGetDataWhere,
    FibGetDataFind,
    FibUpdateData,
} = require("./firebase-fn")();

// #EJS
// app.set("view engine", "ejs");

app.use(cors());
app.use(helmet({ contentSecurityPolicy: false }));
app.use(express.json({ limit: "20mb" }));
app.use(express.urlencoded({ extended: true }));

app.use(
    session({
        secret: "I_X$%SOE32%0!dP@sowo2-1d", // set your secret here
        saveUninitialized: false,
        resave: false,
        cookie: { maxAge: 60000 },
    })
);

// render html ของส่วน user
app.use(express.static(path.join(__dirname, "../client/build")));

// prefix service
app.use("/service", router);

// จ่ายเงิน
// URL DEBIT TEST ID CARD
// https://www.omise.co/api-testing
router.post("/charge", async(req, res) => {
    try {
        const order_id = getOrderId();
        const charge = await omise.charges.create({
            amount: (req.body || {}).sumPrice || 0, //ขั้นต่ำต้อง 20 บาท หรือ 2000 สตางค์
            currency: "thb",
            card: (req.body || {}).omiseToken,
            description: (req.body || {}).paymentDesc || "",
            metadata: {
                order_id,
            },
        });

        req.session.charge_id = charge.id;

        if (charge.paid) {
            res.send({
                status: "200",
                message: "จ่ายเงินสำเร็จ",
                data: charge,
            });
        } else {
            res.send({
                status: "500",
                message: "จ่ายเงินผิดพลาด",
                error: {},
            });
        }
    } catch (error) {
        res.send({
            status: "500",
            message: "จ่ายเงินผิดพลาด",
            error: error,
        });
    }
});

router.post("/send-email", async(req, res) => {
    if (req.body["html"] && req.body["sendTo"]) {
        const { filePath, fileName } = await genHTMLtoImage(req.body["html"]);
        const transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
                // ข้อมูลการเข้าสู่ระบบ
                user: "ashomevr@gmail.com", // email user ของเรา
                pass: "Ashomevr112233", // email password
            },
        });

        transporter.sendMail({
                from: '"Fred Foo 👻" <ashomevr@gmail.com>', // อีเมลผู้ส่ง
                to: req.body["sendTo"], // อีเมลผู้รับ สามารถกำหนดได้มากกว่า 1 อีเมล โดยขั้นด้วย ,(Comma)
                subject: "VR as Home Payment Slip ✔", // หัวข้ออีเมล
                text: "เอกสารการจ่ายเงิน <VR as Home Payment Slip>", // plain text body
                html: "<b>Hello world?</b>", // html body
                attachments: [{
                    filename: fileName,
                    path: filePath,
                    contentType: "application/pdf",
                }, ],
            },
            (err, info) => {
                if (err) {
                    res.send({ error: err });
                }
                res.send(info);
            }
        );
    } else {
        res.send({
            status: "error",
            message: "not html content or sendto email invalid",
        });
    }
});

router.get("/get-address-th", async(req, res) => {
    res.send({
        status: "200",
        message: "success",
        data: dbTH,
    });
});

// จ่ายเงิน สำเร็จ
router.get("/payment-success", async(req, res) => {
    res.send(req.body);
});

router.post("/login", async(req, res) => {
    try {
        if ((req.body && req.body["username"], req.body["password"])) {
            const fibData = await FibLogin(
                req.body["username"],
                req.body["password"]
            );
            res.send({
                status: "200",
                message: "success",
                data: fibData,
            });
        } else {
            res.send({
                status: "200",
                message: "success",
                data: [],
            });
        }
    } catch (err) {
        res.send({
            status: "error",
            message: "error",
            error: err,
        });
    }
});

router.get("/firebase-get", async(req, res) => {
    try {
        if (req.query && req.query["col"]) {
            const fibData = await FibGetDataAll(req.query["col"]);
            res.send({
                status: "200",
                message: "success",
                data: fibData,
            });
        } else {
            res.send({
                status: "200",
                message: "success",
                data: [],
            });
        }
    } catch (err) {
        res.send({
            status: "error",
            message: "error",
            error: err,
        });
    }
});

router.get("/get-product-group", async(req, res) => {
    try {
        if (req.query && req.query["group"].match("ลดราคา")) {
            const fibData = await FibGetDataAll("products");
            const filter = fibData.filter((product) => +product["discount"] > 0);
            res.send({
                status: "200",
                message: "success",
                data: filter,
            });
        } else if (req.query && req.query["group"]) {
            const fibData = await FibGetDataAll("products");
            const filter = fibData.filter((product) =>
                product["group"].includes(req.query["group"])
            );
            res.send({
                status: "200",
                message: "success",
                data: filter,
            });
        } else {
            res.send({
                status: "200",
                message: "success",
                data: [],
            });
        }
    } catch (err) {
        res.send({
            status: "error",
            message: "error",
            error: err,
        });
    }
});

router.get("/get-product-type", async(req, res) => {
    try {
        if (req.query && req.query["productType"]) {
            const fibData = await FibGetDataAll("products");
            const filter = fibData.filter((product) =>
                product["productType"].includes(req.query["productType"])
            );
            res.send({
                status: "200",
                message: "success",
                data: filter,
            });
        } else {
            res.send({
                status: "200",
                message: "success",
                data: [],
            });
        }
    } catch (err) {
        res.send({
            status: "error",
            message: "error",
            error: err,
        });
    }
});

router.get("/search-product-keyword", async(req, res) => {
    try {
        if (req.query && req.query["keyword"]) {
            const fibData = await FibGetDataAll("products");
            let results = [];
            fibData.forEach((obj) => {
                Object.keys(obj).forEach((key) => {
                    if (typeof obj[key] != "object" && typeof obj[key] != "array") {
                        if (
                            `${obj[key]}`
                            .toLowerCase()
                            .indexOf(req.query["keyword"].toLowerCase()) != -1
                        ) {
                            results.push(obj);
                        }
                    }
                });
            });

            res.send({
                status: "200",
                message: "success",
                data: results,
            });
        } else {
            res.send({
                status: "200",
                message: "success",
                data: [],
            });
        }
    } catch (err) {
        res.send({
            status: "error",
            message: "error",
            error: err,
        });
    }
});

router.get("/firebase-get-where", async(req, res) => {
    try {
        if (
            req.query &&
            req.query["col"] &&
            req.query["key"] &&
            req.query["valuefind"]
        ) {
            const fibData = await FibGetDataWhere(
                req.query["col"],
                req.query["key"],
                req.query["valuefind"]
            );
            res.send({
                status: "200",
                message: "success",
                data: fibData,
            });
        } else {
            res.send({
                status: "200",
                message: "success",
                data: [],
            });
        }
    } catch (err) {
        res.send({
            status: "error",
            message: "error",
            error: err,
        });
    }
});

router.get("/firebase-get-find", async(req, res) => {
    try {
        if (
            req.query &&
            req.query["col"] &&
            req.query["key"] &&
            req.query["valuefind"]
        ) {
            const fibData = await FibGetDataFind(
                req.query["col"],
                req.query["key"],
                req.query["valuefind"]
            );
            res.send({
                status: "200",
                message: "success",
                data: fibData,
            });
        } else {
            res.send({
                status: "200",
                message: "success",
                data: [],
            });
        }
    } catch (err) {
        res.send({
            status: "error",
            message: "error",
            error: err,
        });
    }
});

router.post("/firebase-post", async(req, res) => {
    try {
        if (req.body && req.query["col"]) {
            let data = req.body;
            if (req.query["col"] == "users") {
                data = Object.assign({
                        registerDate: Date.now(),
                    },
                    data
                );
            }

            if (req.query["col"] == "billorders") {
                // สร้างการแจ้งเตือน
                await FibAddData("notifys", `${req.query["col"]}_${uuid()}`, {
                    userId: req.body['userId'],
                    timeStamp: Date.now(),
                    readed: false,
                    description: "ส่งใบรายการสั่งซื้อเรียบร้อยแล้ว"
                });
            }

            const fibData = await FibAddData(
                req.query["col"],
                `${req.query["col"]}_${uuid()}`,
                data
            );

            res.send({
                status: "200",
                message: "success",
                data: fibData,
            });
        } else {
            res.send({
                status: "200",
                message: "success",
                data: {},
            });
        }
    } catch (err) {
        res.send({
            status: "error",
            message: "error",
            error: err,
        });
    }
});

router.get("/verify-user", async(req, res) => {
    try {
        if (req.query && req.query["userId"]) {
            const fibData = await FibGetDataFind(
                "users",
                "userId",
                req.query["userId"]
            );
            delete fibData.pass;
            res.send({
                status: "200",
                message: "success",
                data: fibData,
            });
        } else {
            res.send({
                status: "200",
                message: "success",
                data: {},
            });
        }
    } catch (err) {
        res.send({
            status: "error",
            message: "error",
            error: err,
        });
    }
});

router.get("/verify-email", async(req, res) => {
    try {
        if (req.query && req.query["email"]) {
            const fibData = await FibGetDataFind(
                "users",
                "email",
                req.query["email"]
            );
            delete fibData.pass;
            res.send({
                status: "200",
                message: "success",
                data: fibData,
            });
        } else {
            res.send({
                status: "200",
                message: "success",
                data: {},
            });
        }
    } catch (err) {
        res.send({
            status: "error",
            message: "error",
            error: err,
        });
    }
});

router.post("/firebase-del", async(req, res) => {
    try {
        if (req.query && req.query["col"] && req.query["uuid"]) {
            const fibData = await FibDelData(req.query["col"], req.query["uuid"]);
            res.send({
                status: "200",
                message: "success",
                data: fibData,
            });
        } else {
            res.send({
                status: "200",
                message: "success",
                data: {},
            });
        }
    } catch (err) {
        res.send({
            status: "error",
            message: "error",
            error: err,
        });
    }
});

router.post("/firebase-update", async(req, res) => {
    try {
        if (req.body && req.query && req.query["col"] && req.body["uuid"]) {
            let fibData;
            if (req.query["col"] == "users") {
                const userProfile = await FibGetDataFind(
                    req.query["col"],
                    "uuid",
                    req.body["uuid"] || req.query["uuid"]
                );
                fibData = await FibUpdateData(
                    req.query["col"],
                    req.body["uuid"] || req.query["uuid"], {...req.body, pass: req.body["pass"] || userProfile.pass || "" }
                );
            } else {
                fibData = await FibUpdateData(
                    req.query["col"],
                    req.body["uuid"] || req.query["uuid"],
                    req.body
                );
            }

            res.send({
                status: "200",
                message: "success",
                data: fibData,
            });
        } else {
            res.send({
                status: "200",
                message: "success",
                data: {},
            });
        }
    } catch (err) {
        res.send({
            status: "500",
            message: "error",
            error: err,
        });
    }
});

function getOrderId() {
    return Math.round(Math.random() * 1000000 + 1)
        .toString()
        .padStart(7, "0");
}

const port = process.env.PORT || 3000;

// ทำเป็น localhost https
// const httpsServer = https.createServer(credentials, app);
// httpsServer.listen(port);

app.listen(port, "0.0.0.0", () => {
    console.log(`Example app listening at http://localhost:${port}`);
});

// #FIX puppeteer ERROR https://github.com/puppeteer/puppeteer/issues/5662
// puppeteer ซัพพอร์ท ภาษาไทยบน ubuntu $ sudo apt install xfonts-thai
async function genHTMLtoImage(html) {
    const browser = await puppeteer.launch({
        headless: true,
        args: [
            "--no-sandbox",
            "--disable-setuid-sandbox",
            "--font-render-hinting=none",
        ],
    });
    const page = await browser.newPage();
    await page.goto(`data:text/html;charset=utf-8,${html}`);
    const fileName = `/payment_slip_${Date.now()}.pdf`;
    const pathPdf = `${__dirname}/pdf_files/${fileName}`;
    // or a .pdf file
    await page.pdf({
        format: "A4",
        path: pathPdf,
        printBackground: true,
        displayHeaderFooter: true,
    });

    await browser.close();

    return {
        filePath: pathPdf,
        fileName: fileName,
    };
}