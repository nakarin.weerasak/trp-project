export function inputPattern(strInput, type) {
  let respStrPat = "";
  const inputReplace = strInput.split("-").join("");
  const patt = {idcard: "*-****-*****-**-*", tel: "***-***-****", tel2: "**-***-****"};
  if (inputReplace) {
    let count = 0;
    [...patt[type]].forEach((_pat) => {
      if (_pat != "-") {
        respStrPat += [...inputReplace][count] || "X";
        count++;
      } else {
        respStrPat += "-";
      }
    });
  }
  return respStrPat;
}
