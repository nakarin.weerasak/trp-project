const prex = (import.meta as any).env.SNOWPACK_PUBLIC_API_PREFIX;

export const APIPath = {
    login: `${prex}/login`,
    verifyUser: `${prex}/verify-user`,
    verifyEmail: `${prex}/verify-email`,
    paymentOmise: `${prex}/charge`,
    firebasePost: `${prex}/firebase-post`,
    firebaseGet: `${prex}/firebase-get`,
    firebaseUpdate: `${prex}/firebase-update`,
    firebaseGetFind: `${prex}/firebase-get-find`, // ได้ออกมาค่าเดียวเป็น object {}
    firebaseGetWhere: `${prex}/firebase-get-where`, // ได้ออกมาเป็น Array  [{}, {}]
    getAddressTH: `${prex}/get-address-th`,
    getProducts: `${prex}/firebase-get?col=products`,
    getProductByGroup: `${prex}/get-product-group`,
    getProductByType: `${prex}/get-product-type`,
    searchProductKeyword: `${prex}/search-product-keyword`,
    sendSlipToEmail: `${prex}/send-email`,
}
