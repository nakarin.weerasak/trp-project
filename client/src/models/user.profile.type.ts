import type { AddressModel } from "./address.type";

export type UserProfileModel = {
    uuid?: string;
    userId: string;
    loginBy: string;
    fname: string;
    sex: string;
    lname: string;
    pass: string;
    birth: string;
    email: string;
    image: string;
    telNo: string;
    address: AddressModel[];
    status: string;
}

export function UserProfileModeltoJson(val: any): UserProfileModel {
    const jv: UserProfileModel = JSON.parse(val);
    return jv;
}