export type FacebookAuthModel = {
    id: string;
    imgProfile: string;
    name: string;
    email?: string;
}

export function FacebookAuthModeltoJson(val: any): FacebookAuthModel {
    const jv: FacebookAuthModel = JSON.parse(val);
    return jv;
}