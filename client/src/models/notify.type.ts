export type NotifyModel = {
    uuid: string;
    userId: string;
    description: string;
    timeStamp: number;
    readed: boolean;
}
