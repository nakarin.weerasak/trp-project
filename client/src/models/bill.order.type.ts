import type { AddressModel } from "./address.type";
import type { CartProductModel } from "./cart.product.type";

export type BillOrderModel = {
    billId: string;
    cardInfo?: any;
    userId: string;
    transactionPaymentId?: string;
    statusPayment?: string;
    addressDelivery?:  AddressModel;
    createTime?: string;
    productList: CartProductModel[];
    sumPriceBill: number
    statusOrder: string;

}

export function BillOrderModeltoJson(val: any): BillOrderModel {
    const jv: BillOrderModel = JSON.parse(val);
    return jv;
}