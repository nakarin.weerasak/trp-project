export type AddressModel = {
    id: number;
    fullname: string;
    mobilePhone: string;
    address: string;
    subdistrict: string;
    district: string;
    province: string;
    zipcode: string;
}
