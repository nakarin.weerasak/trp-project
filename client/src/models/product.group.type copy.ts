export type ProductGroupModel = {
    id: string; // รหัส Type
    icon: string; // logo 
    querySearch: string;
    typeName: string; // ชื่อกลุ่ม
    showFor: string; // แสดงใน userStandard ปกติ หรือ userRegister หรือ all
}

