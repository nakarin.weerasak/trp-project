export type ProductTypeModel = {
    id: string; // รหัส Type
    icon: string;
    typeName: string; // ชื่อประเภท
}

