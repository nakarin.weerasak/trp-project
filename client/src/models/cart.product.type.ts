import type { ProductOptionModel } from "./product.type";

export type CartProductModel = {
    id: string
    productId: string;
    productName: string;
    optionSelect: ProductOptionModel;
    image: string;
    desc: string;
    discount: number;
    price: number;
    selected: boolean;
    amount: number;
    sumPrice: number;
}


export function CartProductModeltoJson(val: any): CartProductModel[] {
    const jv: CartProductModel[] = JSON.parse(val);
    return jv;
}