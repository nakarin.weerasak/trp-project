export function axiosHeaderOptions() {
    const bearerKey = (import.meta as any).env.SNOWPACK_PUBLIC_BEARER_KEY || 'jwt_token';
    if (getCookie(bearerKey)) {
        return {
            headers: {
                Authorization: `Bearer ${getCookie(bearerKey)}`
            }
        }
    } else {
        return {
            headers: {
                Authorization: ``
            }
        }
    }

}

function getCookie(name: string) {
    const document = (window as any).document;
    var cookieArr = document.cookie.split(";");

    // Loop through the array elements
    for (var i = 0; i < cookieArr.length; i++) {
        var cookiePair = cookieArr[i].split("=");

        /* Removing whitespace at the beginning of the cookie name
        and compare it with the given string */
        if (name == cookiePair[0].trim()) {
            // Decode the cookie value and return
            return decodeURIComponent(cookiePair[1]);
        }
    }

    // Return null if not found
    return null;
}
