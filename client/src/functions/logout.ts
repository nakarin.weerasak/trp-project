export function logoutSession() {
    const cookies = (window as any).document.cookie.split(";");

    (window as any).localStorage.clear();

    for (let i = 0; i < cookies.length; i++) {
        let cookie = cookies[i];
        let eqPos = cookie.indexOf("=");
        let name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
    (window as any).location.reload();
}