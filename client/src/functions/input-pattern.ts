export function inputPattern(strInput: string, type: string) {
    let respStrPat = "";
    const inputReplace = strInput.split('-').join("");
    const patt: any = { "idcard": "*-****-*****-**-*", "tel": "***-***-****", "tel2": "**-***-****" };
    if (inputReplace) {
        let count = 0;
        [...patt[type]].forEach((_pat) => {
            if(_pat != "-") {
                respStrPat += [...inputReplace][count] || "X"
                count++;
            } else {
                respStrPat += "-"
            }
        })
    }

    return respStrPat;
}