import axios from 'axios';
import { newsStore, showLazyLoading } from 'src/store';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';
import { Collection } from 'src/collectionFirebase';

export async function getNewsAPI() {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.get(`${APIPath.firebaseGet}?col=${Collection.NEWS}`, axiosHeaderOptions());
        showLazyLoading.set(false);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            newsStore.set(respAPI?.data?.data || []);
        }
    } catch (error) {
        showLazyLoading.set(false);
    }
}
