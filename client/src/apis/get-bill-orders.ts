import axios from 'axios';
import { billOrderListStore, showLazyLoading } from 'src/store';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';
import { Collection } from 'src/collectionFirebase';

export async function getBillOrdersAPI(userId: string) {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.get(`${APIPath.firebaseGetWhere}?col=${Collection.BILLORDERS}&key=userId&valuefind=${userId}`, axiosHeaderOptions());
        showLazyLoading.set(false);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            billOrderListStore.set(respAPI?.data?.data)
        }
    } catch (error) {
        showLazyLoading.set(false);
    }
}
