import axios from 'axios';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';

export async function verifyUserIdExistsAPI(userId: string) {
    try {
        const respAPI: any = await axios.get(`${APIPath.verifyUser}?userId=${userId}`, axiosHeaderOptions());
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            return respAPI?.data?.data ;
        } else {
            return {};
        }
    } catch (error) {
        return {};
    }
}
