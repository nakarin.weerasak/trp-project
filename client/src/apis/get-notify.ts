import axios from 'axios';
import { notifyStore, showLazyLoading } from 'src/store';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';
import { Collection } from 'src/collectionFirebase';

export async function getNotifyAPI(userId: string) {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.get(`${APIPath.firebaseGet}?col=${Collection.NOTIFYS}&userId=${userId}`, axiosHeaderOptions());
        showLazyLoading.set(false);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            // เรียงจากเวลาใกล้สุด ไปไกลสุด
            const filterByDate = respAPI?.data?.data.sort(
                (a: any, b: any) => +new Date(`${b.timeStamp}`) - +new Date(`${a.timeStamp}`)
            );
            notifyStore.set(filterByDate || [])
        }
    } catch (error) {
        showLazyLoading.set(false);
    }
}
