import axios from 'axios';
import { showLazyLoading, productsStore } from 'src/store';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';

export async function getProductByTypeAPI(params: string) {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.get(`${APIPath.getProductByType}?productType=${params}`, axiosHeaderOptions());
        showLazyLoading.set(false);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            productsStore.set(respAPI?.data?.data || []);
        }
    } catch (error) {
        showLazyLoading.set(false);
    }
}
