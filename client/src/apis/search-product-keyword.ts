import axios from 'axios';
import { showLazyLoading, productsStore } from 'src/store';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';

export async function searchProductKeywordAPI(keyword: string) {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.get(`${APIPath.searchProductKeyword}?keyword=${keyword}`, axiosHeaderOptions());
        showLazyLoading.set(false);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            productsStore.set(respAPI?.data?.data || []);
        } 
    } catch (error) {
        showLazyLoading.set(false);
    }
}
