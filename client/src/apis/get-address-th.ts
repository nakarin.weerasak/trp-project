import axios from 'axios';
import { showLazyLoading, addressTHStore } from 'src/store';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';

export async function getAddressTHAPI() {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.get(`${APIPath.getAddressTH}`, axiosHeaderOptions());
        showLazyLoading.set(false);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            addressTHStore.set(respAPI?.data?.data || []);
        }
    } catch (error) {
        showLazyLoading.set(false);
    }
}
