import axios from 'axios';
import { showLazyLoading, userFormRegisStore } from 'src/store';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';
import type { UserProfileModel } from 'src/models/user.profile.type';
import { Collection } from 'src/collectionFirebase';

export async function postUserProfileAPI(userProfile: UserProfileModel) {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.post(`${APIPath.firebasePost}?col=${Collection.USERS}`, userProfile, axiosHeaderOptions());
        showLazyLoading.set(false);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            userFormRegisStore.set(respAPI?.data || {});
            return respAPI?.data;
        } else {
            return respAPI?.data;
        }
    } catch (error) {
        showLazyLoading.set(false);
        return {}
    }
}
