import axios from 'axios';
import { showLazyLoading, userFormRegisStore } from 'src/store';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';

export async function sendSlipToEmailAPI(html: string, sendTo: string) {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.post(`${APIPath.sendSlipToEmail}`, {
            "html": html,
            "sendTo": sendTo
        }, axiosHeaderOptions());
        showLazyLoading.set(false);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            userFormRegisStore.set(respAPI?.data || {});
            return respAPI?.data;
        } else {
            return respAPI?.data;
        }
    } catch (error) {
        showLazyLoading.set(false);
        return {}
    }
}
