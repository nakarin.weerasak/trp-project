import axios from 'axios';
import {  billOrderStore, showLazyLoading } from 'src/store';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';
import { Collection } from 'src/collectionFirebase';
import type { BillOrderModel } from 'src/models/bill.order.type';

export async function postBillOrderAPI(billOrders: BillOrderModel, sumPrice: number, omiseToken: string, omiseSource: string) {
    try {
        showLazyLoading.set(true);
        const respPayment: any = await axios.post(`${APIPath.paymentOmise}`, {
            "paymentDesc": billOrders.billId,
            "sumPrice": sumPrice,
            "omiseToken": omiseToken,
            "omiseSource": omiseSource
        }, axiosHeaderOptions());

        const paymentData = respPayment?.data?.data || {};
        if (paymentData && Object.values(paymentData).length) {
            billOrders.cardInfo = paymentData?.card || {};
            billOrders.statusPayment = paymentData?.status || "error";
            billOrders.transactionPaymentId = paymentData?.transaction || ""
            billOrders.createTime = new Date().toLocaleString();

            billOrderStore.set(billOrders);
        }

        const respAPI: any = await axios.post(`${APIPath.firebasePost}?col=${Collection.BILLORDERS}`, billOrders, axiosHeaderOptions());
        showLazyLoading.set(false);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            return respAPI?.data?.data;
        } else {
            return "error";
        }
    } catch (error) {
        showLazyLoading.set(false);
        return error;
    }
}
