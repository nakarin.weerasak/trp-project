import axios from 'axios';
import { notifyStore, showLazyLoading } from 'src/store';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';
import { Collection } from 'src/collectionFirebase';
import type { NotifyModel } from 'src/models/notify.type';

export async function updateNotifyAPI(notify: NotifyModel) {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.post(`${APIPath.firebaseUpdate}?col=${Collection.NOTIFYS}&uuid=${notify.uuid}`, notify, axiosHeaderOptions());
        showLazyLoading.set(false);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            return respAPI?.data;
        } else {
            return {}
        }
    } catch (error) {
        showLazyLoading.set(false);
    }
}
