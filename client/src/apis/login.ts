import axios from 'axios';
import { APIPath } from 'src/api-path';
import { showLazyLoading, userProfileStore } from 'src/store';

export async function loginAPI(_username: String, _password: String) {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.post(APIPath.login, { username: _username, password: _password });
        showLazyLoading.set(false);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            userProfileStore.set(respAPI?.data?.data || {});
        } else {
            return { "message": "* Incorrect username or password" };
        }
    } catch (error) {
        showLazyLoading.set(false);
        return { "message": "* Incorrect username or password" };
    }
}
