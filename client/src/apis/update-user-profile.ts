import axios from 'axios';
import { showLazyLoading, userFormRegisStore } from 'src/store';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';
import type { UserProfileModel } from 'src/models/user.profile.type';
import { Collection } from 'src/collectionFirebase';

export async function updateUserProfileAPI(userProfile: UserProfileModel) {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.post(`${APIPath.firebaseUpdate}?col=${Collection.USERS}&uuid=${userProfile.uuid}`, userProfile, axiosHeaderOptions());
        showLazyLoading.set(false);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            userFormRegisStore.set(respAPI?.data || {});
        }
    } catch (error) {
        showLazyLoading.set(false);
    }
}
