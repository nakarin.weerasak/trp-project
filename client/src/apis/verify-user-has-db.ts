import axios from 'axios';
import { showLazyLoading, userProfileStore } from 'src/store';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';

export async function verifyUserHasDBAPI(userId: string) {
    try {
        showLazyLoading.set(true);
        const respAPI: any = await axios.get(`${APIPath.verifyUser}?userId=${userId}`, axiosHeaderOptions());
        showLazyLoading.set(false);
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            userProfileStore.set(respAPI?.data?.data || {});
        } 
    } catch (error) {
        showLazyLoading.set(false);
    }
}
