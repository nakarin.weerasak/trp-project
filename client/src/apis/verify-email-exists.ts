import axios from 'axios';
import { APIPath } from 'src/api-path';
import { axiosHeaderOptions } from 'src/axios-header-options';

export async function verifyEmailExistsAPI(email: string) {
    try {
        const respAPI: any = await axios.get(`${APIPath.verifyEmail}?email=${email}`, axiosHeaderOptions());
        if (`${(respAPI?.data || {})["status"]}`.match(/^2/)) {
            return respAPI?.data?.data ;
        } else {
            return {};
        }
    } catch (error) {
        return {};
    }
}
