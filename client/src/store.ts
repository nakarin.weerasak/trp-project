import { writable } from "svelte/store";
import { BillOrderModel, BillOrderModeltoJson } from "./models/bill.order.type";
import { CartProductModel, CartProductModeltoJson } from "./models/cart.product.type";
import { FacebookAuthModel, FacebookAuthModeltoJson } from "./models/facebook.auth";
import { UserProfileModel, UserProfileModeltoJson } from "./models/user.profile.type";
import type { ProductModel } from "./models/product.type";
import type { UserAuthModel } from "./models/user.auth.type";
import type { ProductGroupModel } from "./models/product.group.type";
import type { ProductTypeModel } from "./models/product.type.type";
import type { NotifyModel } from "./models/notify.type";
export const showLazyLoading = writable(false);
export const isAuthSuccess = writable(false);
export const userAuthStore = writable<UserAuthModel>(<UserAuthModel>{});
export const facebookAuthStore = writable<FacebookAuthModel>(<FacebookAuthModel>{});
export const userFormRegisStore = writable<UserProfileModel>(<UserProfileModel>{});
export const addressTHStore = writable([]);
export const userProfileStore = writable<UserProfileModel>(<UserProfileModel>{});
export const pageSelectedStore = writable<string>("");
export const pageBackStore = writable<string>("");
export const cartOrdersStore = writable<CartProductModel[]>([]);
export const productGroupStore = writable<ProductGroupModel[]>([]);
export const productTypeStore = writable<ProductTypeModel[]>([]);
export const billOrderStore = writable<BillOrderModel>(<BillOrderModel>{});
export const billOrderListStore = writable<BillOrderModel[]>([]);
export const productsStore = writable<ProductModel[]>([]);
export const productSelectedStore = writable<ProductModel>(<ProductModel>{});
export const notifyStore = writable<NotifyModel[]>([]);

export const newsStore = writable<any[]>([]);

// Local Store Set

userProfileStore.set(UserProfileModeltoJson(localStorage.getItem("userProfileStore")));
userProfileStore.subscribe((val: any) => {
    if (val && Object.values(val).length) {
        localStorage.setItem("userProfileStore", JSON.stringify(val));
    }
});

facebookAuthStore.set(FacebookAuthModeltoJson(localStorage.getItem("facebookAuthStore")));
facebookAuthStore.subscribe((val: any) => {
    if (val && Object.values(val).length) {
        localStorage.setItem("facebookAuthStore", JSON.stringify(val));
    }
});


cartOrdersStore.set(CartProductModeltoJson(localStorage.getItem("cartOrdersStore")));
cartOrdersStore.subscribe((val: any) => {
    if (val && val.length) {
        localStorage.setItem("cartOrdersStore", JSON.stringify(val));
    } else {
        localStorage.removeItem("cartOrdersStore");
    }
});


billOrderStore.set(BillOrderModeltoJson(localStorage.getItem("billOrderStore")));
billOrderStore.subscribe((val: any) => {
    if (val && Object.values(val).length) {
        localStorage.setItem("billOrderStore", JSON.stringify(val));
    } else {
        localStorage.removeItem("billOrderStore");
    }
});


pageSelectedStore.set(localStorage.getItem("pageSelectedStore") || "");
pageSelectedStore.subscribe((val: any) => {
    if (val) {
        localStorage.setItem("pageSelectedStore", val);
    } else {
        localStorage.removeItem("pageSelectedStore");
    }
});


pageBackStore.set(localStorage.getItem("pageBackStore") || "");
pageBackStore.subscribe((val: any) => {
    if (val) {
        localStorage.setItem("pageBackStore", val);
    } else {
        localStorage.removeItem("pageBackStore");
    }
});
